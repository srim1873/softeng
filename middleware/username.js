const jwt = require('jsonwebtoken');

const secret = '1c28d07215544bd1b24fac3242352cacacad6c14a04';

module.exports = (requset) => {
  const data = requset.cookies.token;
  let username = null;
  jwt.verify(data, secret, (err, payload) => {
    if (payload) {
      username = payload.user;
    }
  });
  return username;
};
