/* eslint-disable complexity */
exports.validate = () => (request, res, next) => {
  const city = `${request.body.varos}`;
  const quarter = `${request.body.negyed}`;
  const price = `${request.body.ar}`;
  const room = `${request.body.szoba}`;
  const area = `${request.body.terulet}`;
  const user = `${request.body.tulaj}`;

  if (!(/[A-ZÁÉÚŐÓÜÖÍ][a-z]*/.test(city) && city.length !== 0)) {
    res.status(400).json({ message: 'Incorrect or missing city' });
  }

  // negyed nev validalas
  if (!(/[A-ZÁÉÚŐÓÜÖÍ][a-z]*/.test(quarter) && quarter.length !== 0)) {
    res.status(400).json({ message: 'Incorrect or missing quarter' });
  }

  // ar validalas
  if (!(/[1-9][0-9]*/.test(price) && price.length !== 0)) {
    res.status(400).json({ message: 'Incorrect or missing price' });
  }

  // szoba validalas
  if (!(/[1-9][0-9]*/.test(room) && room.length !== 0)) {
    res.status(400).json({ message: 'Incorrect or missing room' });
  }

  // terulet validalad
  if (!(/[1-9][0-9]*/.test(area) && area.length !== 0)) {
    res.status(400).json({ message: 'Incorrect or missing area' });
  }

  if (user.length === 0) {
    res.status(400).json({ message: 'Incorrect or missing user' });
  }

  next();
};
