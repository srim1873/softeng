const jwt = require('jsonwebtoken');

const secret = '1c28d07215544bd1b24fac3242352cacacad6c14a04';

module.exports = (roles = ['user', 'admin']) => (req, res, next) => {
  const data = req.cookies.token;
  if (!data) {
    res.render('error', { message: 'Csak felhasználóknak van engedélye. Jelentkezzen be vagy regisztráljon.' });
    return;
  }
  jwt.verify(data, secret, (err, payload) => {
    if (payload) {
      const role = (payload.isadmin === true) ? 'admin' : 'user';
      if (!roles.includes(role)) {
        res.render('error', { message: 'Csak speciális szerepkörű felhasználónak megengedett.' });
      } else {
        next();
      }
    } else {
      res.render('error', { message: 'Csak felhasználóknak van engedélye. Jelentkezzen be vagy regisztráljon.' });
    }
  });
};
