
const mongoose = require('mongoose');
const pathfile = require('path');
const getusername = require('../middleware/username');

mongoose.connect('mongodb+srv://srim1873:srim1873@webproject-mzb9s.mongodb.net/test?retryWrites=true&w=majority', { useNewUrlParser: true, useUnifiedTopology: true });
const SubmitInfo = require('./model/hirdetes');
const UserInfo = require('./model/felhasznalo');
const PictureInfo = require('./model/kep');

mongoose.set('useNewUrlParser', true);
mongoose.set('useFindAndModify', false);
mongoose.set('useCreateIndex', true);
mongoose.set('useUnifiedTopology', true);

exports.insertProduct = (request, callback) => {
  const user = getusername(request);
  const today = new Date().toLocaleString();
  const myobj = new SubmitInfo({
    _id: new mongoose.Types.ObjectId(),
    varos: `${request.body.varos}`,
    negyed: `${request.body.negyed}`,
    terulet: `${request.body.terulet}`,
    szoba: `${request.body.szoba}`,
    ar: `${request.body.ar}`,
    feltoltesIdopont: `${today}`,
    tulaj: `${user}`,
  });

  myobj.save().then(callback(null))
    .catch((err) =>  callback(new Error(err)));
};

exports.insertProductforApi = (request, callback) => {
  const today = new Date().toLocaleString();
  const myobj = new SubmitInfo({
    _id: new mongoose.Types.ObjectId(),
    varos: `${request.body.varos}`,
    negyed: `${request.body.negyed}`,
    terulet: `${request.body.terulet}`,
    szoba: `${request.body.szoba}`,
    ar: `${request.body.ar}`,
    feltoltesIdopont: `${today}`,
    tulaj: `${request.body.tulaj}`,
  });

  myobj.save().then(callback(null, myobj))
    .catch((err) =>  callback(new Error(err), null));
};

exports.updateProduct = (id, request, callback) => {
  SubmitInfo.findOne({ _id: id }, (err)  => {
    if (err) {
      callback(err, null);
    } else {
      SubmitInfo.updateOne({ _id: id }, request.body, (error, data) => {
        if (error) {
          callback(new Error(error), null);
        } else {
          callback(null, data);
        }
      });
    }
  });
};

exports.insertPicture = (request, callback) => {
  const id = `${request.fields.hirdID}`;
  const nev = `${request.files.fileToUpload.path}`;
  const filename = pathfile.parse(nev).base;
  const myobj = new PictureInfo({
    _id: new mongoose.Types.ObjectId(),
    kep: filename,
    hivatkozas: `${id}`,
  });
  myobj.save().then(callback(null))
    .catch((err) =>  callback(new Error(err)));
};

exports.insertUser = (request, callback) => {
  let booladmin = false;
  const admin = `${request.body.admin}`;
  if (admin === 'admin') {
    booladmin = true;
  }
  const myobj = new UserInfo({
    _id: new mongoose.Types.ObjectId(),
    email: `${request.body.email}`,
    username: `${request.body.felhasznalo}`,
    password: `${request.body.password}`,
    admin: booladmin,
  });

  myobj.save().then(callback(null))
    .catch((err) =>  callback(new Error(err)));
};

exports.deleteProduct = (currid, callback) => {
  SubmitInfo.findOneAndRemove({ _id: currid }, (err)  => {
    if (err) {
      callback(new Error(err));
    } else {
      callback(null);
    }
  });
};

exports.deletePicture = (currid, callback) => {
  PictureInfo.findOneAndRemove({ _id: currid }, (err)  => {
    if (err) {
      callback(new Error(err));
    } else {
      callback(null);
    }
  });
};

exports.findProduct = (currid, callback) => {
  SubmitInfo.findOne({ _id: currid }, (err, data)  => {
    callback(err, data);
  });
};

exports.findUserByName = (request, callback) => {
  const name = `${request.body.felhasznalo}`;
  UserInfo.findOne({ username: name }, (err, data)  => {
    callback(err, data);
  });
};

exports.findUserByEmail = (request, callback) => {
  const usemail = `${request.body.email}`;
  UserInfo.findOne({ email: usemail }, (err, data)  => {
    callback(err, data);
  });
};

exports.findRealUser = (picid, callback) => {
  PictureInfo.findOne({ _id: picid }, (err, picdata) => {
    const prodid = picdata.hivatkozas;
    if (err) {
      callback(err, picdata);
    } else {
      SubmitInfo.findOne({ _id: prodid }, (proderr, data)  => {
        callback(proderr, data);
      });
    }
  });
};

exports.findPicture = (id, callback) => {
  PictureInfo.find({ hivatkozas: id }, (err, data) => {
    callback(err, data);
  });
};

exports.findAllProducts = (request, callback) => {
  SubmitInfo.find({}, (err, data) => {
    callback(err, data);
  });
};

exports.findByCity = (request, callback) => {
  const city = `${request.query.varosSearch}`;
  SubmitInfo.find({ varos: city }, (err, data) => {
    callback(err, data);
  });
};


exports.findByQuarter = (request, callback) => {
  const quarter = `${request.query.negyedSearch}`;
  SubmitInfo.find({ negyed: quarter }, (err, data) => {
    callback(err, data);
  });
};

exports.findByMinPrice = (request, callback) => {
  const minp = `${request.query.arMin}`;
  SubmitInfo.find({ ar: { $gte: parseInt(minp, 10) } }, (err, data) => {
    callback(err, data);
  });
};

exports.findByMaxPrice = (request, callback) => {
  const maxp = `${request.query.arMax}`;
  SubmitInfo.find({ ar: { $lte: parseInt(maxp, 10) } }, (err, data) => {
    callback(err, data);
  });
};

exports.findByCityQuarter = (request, callback) => {
  const city = `${request.query.varosSearch}`;
  const quarter = `${request.query.negyedSearch}`;
  SubmitInfo.find({ $and: [{ varos: city }, { negyed: quarter }] }, (err, data) => {
    callback(err, data);
  });
};

exports.findByCityMinPrice = (request, callback) => {
  const city = `${request.query.varosSearch}`;
  const minp = `${request.query.arMin}`;
  SubmitInfo.find({
    $and: [
      { varos: city },
      { ar: { $gte: parseInt(minp, 10) } },
    ],
  }, (err, data) => {
    callback(err, data);
  });
};

exports.findByCityMaxPrice = (request, callback) => {
  const city = `${request.query.varosSearch}`;
  const maxp = `${request.query.arMax}`;
  SubmitInfo.find({
    $and: [
      { varos: city },
      { ar: { $lte: parseInt(maxp, 10) } },
    ],
  }, (err, data) => {
    callback(err, data);
  });
};

exports.findByQuarterMinPrice = (request, callback) => {
  const quarter = `${request.query.negyedSearch}`;
  const minp = `${request.query.arMin}`;
  SubmitInfo.find({
    $and: [
      { negyed: quarter },
      { ar: { $gte: parseInt(minp, 10) } },
    ],
  }, (err, data) => {
    callback(err, data);
  });
};

exports.findByQuarterMaxPrice = (request, callback) => {
  const quarter = `${request.query.negyedSearch}`;
  const maxp = `${request.query.arMax}`;
  SubmitInfo.find({
    $and: [
      { negyed: quarter },
      { ar: { $lte: parseInt(maxp, 10) } },
    ],
  }, (err, data) => {
    callback(err, data);
  });
};

exports.findByMinPriceMaxPrice = (request, callback) => {
  const minp = `${request.query.arMin}`;
  const maxp = `${request.query.arMax}`;
  SubmitInfo.find({
    ar: { $lte: parseInt(maxp, 10), $gte: parseInt(minp, 10) },
  }, (err, data) => {
    callback(err, data);
  });
};

exports.findByCityQuarterMinPrice = (request, callback) => {
  const city = `${request.query.varosSearch}`;
  const quarter = `${request.query.negyedSearch}`;
  const minp = `${request.query.arMin}`;
  SubmitInfo.find({
    $and: [
      { ar: { $gte: parseInt(minp, 10) } },
      { negyed: quarter },
      { varos: city },
    ],
  }, (err, data) => {
    callback(err, data);
  });
};

exports.findByCityQuarterMaxPrice = (request, callback) => {
  const city = `${request.query.varosSearch}`;
  const quarter = `${request.query.negyedSearch}`;
  const maxp = `${request.query.arMax}`;
  SubmitInfo.find({
    $and: [
      { ar: { $lte: parseInt(maxp, 10) } },
      { negyed: quarter },
      { varos: city },
    ],
  }, (err, data) => {
    callback(err, data);
  });
};

exports.findByCityMinPriceMaxPrice = (request, callback) => {
  const city = `${request.query.varosSearch}`;
  const minp = `${request.query.arMin}`;
  const maxp = `${request.query.arMax}`;
  SubmitInfo.find({
    $and: [
      { ar: { $lte: parseInt(maxp, 10), $gte: parseInt(minp, 10) } },
      { varos: city },
    ],
  }, (err, data) => {
    callback(err, data);
  });
};

exports.findByQuarterMinPriceMaxPrice = (request, callback) => {
  const quarter = `${request.query.negyedSearch}`;
  const minp = `${request.query.arMin}`;
  const maxp = `${request.query.arMax}`;
  SubmitInfo.find({
    $and: [
      { ar: { $lte: parseInt(maxp, 10), $gte: parseInt(minp, 10) } },
      { negyed: quarter },
    ],
  }, (err, data) => {
    callback(err, data);
  });
};

exports.findByAll = (request, callback) => {
  const city = `${request.query.varosSearch}`;
  const quarter = `${request.query.negyedSearch}`;
  const minp = `${request.query.arMin}`;
  const maxp = `${request.query.arMax}`;
  SubmitInfo.find({
    $and: [
      { ar: { $lte: parseInt(maxp, 10), $gte: parseInt(minp, 10) } },
      { negyed: quarter },
      { varos: city },
    ],
  }, (err, data) => {
    callback(err, data);
  });
};
