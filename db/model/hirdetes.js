const mongoose = require('mongoose');

const submitSchema = mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  varos: String,
  negyed: String,
  terulet: Number,
  szoba: Number,
  ar: Number,
  feltoltesIdopont: String,
  tulaj: String,
});

module.exports = mongoose.model('SubmitInfo', submitSchema, 'Hirdetes');
