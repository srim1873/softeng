const mongoose = require('mongoose');
// const SubmitInfo = require('./hirdetes.js');

const pictureSchema = mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  kep: String,
  hivatkozas: mongoose.Schema.Types.ObjectId,
});
module.exports = mongoose.model('PictureInfo', pictureSchema, 'Kepek');
