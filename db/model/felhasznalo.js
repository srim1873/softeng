const mongoose = require('mongoose');

const userSchema = mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  email: String,
  username: String,
  password: String,
  admin: Boolean,
});
module.exports = mongoose.model('UserInfo', userSchema, 'Felhasznalok');
