/* eslint-disable max-len */
const express = require('express'),
  db = require('../db/db'),
  validate = require('../middleware/validate');

const router = express.Router();


// findByCity
router.get('/', (req, response, next) => {
  if (`${req.query.varosSearch}` !== 'undefined') {
    db.findByCity(req, (err, data) => {
      if (err) {
        response.status(500).json({ message: `Error while finding products in the city ${req.varosSearch}: ${err.message}` });
      }
      if (data.length === 0) {
        response.status(404).json({ message: 'No result.' });
      } else {
        response.json(data);
      }
    });
  } else {
    next();
  }
});

// findAll products
router.get('/', (request, response) => {
  db.findAllProducts(request, (error, data) => {
    if (error) {
      response.status(500).json({ message: `Error while finding all products: ${error.message}` });
    } else {
      response.json(data);
    }
  });
});

// findById
router.get('/:id', (req, response) => {
  const { id } = req.params;
  db.findProduct(id, (findProdErr, productdata) => {
    if (findProdErr) {
      response.status(500).json({ message: `Error while finding product with ID ${id}: ${findProdErr.message}` });
    } else if (productdata === null) {
      response.status(404).json({ message: `Product with ID ${id} not found.` });
    } else {
      response.json(productdata);
    }
  });
});

// delete by ID
router.delete('/:id', (req, response) => {
  const { id } = req.params;
  db.findProduct(id, (findProdErr, productdata) => {
    if (findProdErr) {
      response.status(500).json({ message: `Error while deleting with ID ${id}: ${findProdErr.message}` });
    } else if (productdata === null) {
      response.status(404).json({ message: `Product with ID ${id} not found.` });
    } else {
      db.deleteProduct(id, (err) => {
        if (err) {
          response.status(500).json({ message: `Error while deleting with ID ${id}: ${err.message}` });
        } else {
          response.sendStatus(204);
        }
      });
    }
  });
});

// insert product
// body-ban megadando:

// varos: varosnev
// negyed:  negyednev
// terulet: terulet
// szoba: szobaszam
// ar: ar
// tulaj: user

router.post('/', validate.validate(), (req, res) => {
  db.insertProductforApi(req, (err, data) => {
    if (err) {
      res.status(500).json({ message: `Error while creating product: ${err.message}` });
    } else {
      res.status(201).location(`${req.fullUrl}/${data.id}`).json(data);
    }
  });
});

// update product
router.patch('/:id', (req, res) => {
  const { id } = req.params;
  db.updateProduct(id, req, (err, data) => {
    if (err) {
      res.status(500).json({ message: `Error while updating product: ${err.message}` });
    } else if (data === null) {
      res.status(404).json({ message: `Product with ID ${id} not found.` });
    } else {
      res.sendStatus(204);
    }
  });
});

module.exports = router;
