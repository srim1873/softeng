// Ez az állomány egy REST API gyökerének express routerét konfigurálja.
// Betölti a további API endpoint lekezelőket.
const express = require('express'),
  morgan = require('morgan'),
  bodyParser = require('body-parser'),
  fullUrlMiddleware = require('../middleware/fullurl'),
  productRoutes = require('./products');

const router = express.Router();

// loggoljuk csak az API kéréseket
router.use(morgan('tiny'));

// tároljuk minden hívás teljes URL-jét
// a HATEOAS kialakításáért
router.use(fullUrlMiddleware);

// minden testtel ellátott API hívás
// JSON-t tartalmaz
router.use(bodyParser.json());

// API endpointok a megfelelő alrouterbe
router.use('/products', productRoutes);

module.exports = router;
