/* eslint-disable no-unused-vars */

// --------------------------------------------------------------------------
// display


function close() {
  document.getElementById('productForm').style.visibility = 'hidden';
  document.getElementById('pictureForm').style.visibility = 'hidden';
  document.getElementById('searchForm').style.visibility = 'hidden';
  document.getElementById('loginForm').style.visibility = 'hidden';
}

function hirdet() {
  document.getElementById('form').style.visibility = 'visible';
}

function closeForm() {
  close();
  document.getElementById('productForm').reset();
  document.getElementById('pictureForm').reset();
  document.getElementById('searchForm').reset();
  document.getElementById('loginForm').reset();
}

function ujHirdetes() {
  close();
  document.getElementById('productForm').style.visibility = 'visible';
}


function hirdetesExit() {
  document.getElementById('form').style.visibility = 'hidden';
}


function ujKep() {
  close();
  document.getElementById('pictureForm').style.visibility = 'visible';
}


function keres() {
  close();
  document.getElementById('searchForm').style.visibility = 'visible';
}

// login
function login() {
  close();
  document.getElementById('loginForm').style.visibility = 'visible';
}


// -----------------------------------------------------------------------------
// validation


// form1 --check if all the form areas all completed

let cityOk;
let quarterOk;
let areaOk;
let priceOk;
let roomOk;

function validateCity() {
  const city = document.getElementById('varos').value;
  if (city.length !== 0) {
    cityOk = 1;
  }
}

function validateQuarter() {
  const quart = document.getElementById('negyed').value;
  if (quart.length !== 0) {
    quarterOk = 1;
  }
}

function validateArea() {
  const area = document.getElementById('terulet').value;
  if (area.length !== 0) {
    areaOk = 1;
  }
}

function validatePrice() {
  const price = document.getElementById('ar').value;
  if (price.length !== 0) {
    priceOk = 1;
  }
}

function validateRoom() {
  const room = document.getElementById('szoba').value;
  if (room.length !== 0) {
    roomOk = 1;
  }
}


function validatePost() {
  const error = document.getElementById('error');
  cityOk = 0;
  quarterOk = 0;
  areaOk = 0;
  priceOk = 0;
  roomOk = 0;
  validateCity();
  validateQuarter();
  validateArea();
  validatePrice();
  validateRoom();

  if (cityOk === 1 && quarterOk === 1 && areaOk === 1 && priceOk === 1 && roomOk === 1) {
    document.getElementById('submit').disabled = false;
    error.innerHTML = '';
  } else {
    error.innerHTML = 'Minden *-al jelölt mező kitöltése kötelező.';
  }
}


//----------------------------------------------------------------------------

// eslint-disable-next-line complexity
function validateData() {
  const email = document.getElementById('email').value;
  const username = document.getElementById('felhasznalo').value;
  const password = document.getElementById('password').value;
  const passwordagain = document.getElementById('passwordagain').value;
  if (email.length === 0 || username.length === 0
    || password.length === 0 || passwordagain.length === 0) {
    return 1;
  }

  if (password.length < 8) {
    return 5;
  }

  if (password.localeCompare(passwordagain) !== 0) {
    return 4;
  }

  if (!username.match('^[A-Za-z0-9]+$')) {
    return 3;
  }

  if (!/^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/.test(email)) {
    return 2;
  }

  return 0;
}
// register form validacio
function validateUser() {
  const button = document.getElementById('feltolt');
  const error = document.getElementById('error');
  const fail = validateData();
  if (fail === 0) {
    button.disabled = false;
    error.innerHTML = '';
  } else if (fail === 1) {
    error.innerHTML = 'Minden *-al jelölt mező kitöltése kötelező.';
    button.disabled = true;
  } else if (fail === 2) {
    error.innerHTML = 'Az email cím formátuma helytelen.';
    button.disabled = true;
  } else if (fail === 3) {
    error.innerHTML = 'A felhasználonév formátuma helytelen. Csak számokat és betűket tartalmazhat.';
    button.disabled = true;
  } else if (fail === 4) {
    error.innerHTML = 'A két jelszó nem egyezik.';
    button.disabled = true;
  } else if (fail === 5) {
    error.innerHTML = 'A jelszó legalább 8 karakter hosszúságú kell legyen.';
    button.disabled = true;
  }
}


function reszletek(id) {
  fetch(`/products/${id}`).then((response) => {
    document.getElementById(`szobaszam${id}`).style.display = 'block';
    document.getElementById(`feltido${id}`).style.display = 'block';
    if (response.status === 500 || response.status === 404) {
      document.getElementById(`szobaszam${id}`).innerHTML = `Szobák száma: hiba történt ${response.message}`;
      document.getElementById(`feltido${id}`).innerHTML = `Feltöltés időpontja: hiba történt ${response.message}`;
    } else {
      document.getElementById(`szobaszam${id}`).innerHTML = `Szobák száma: ${response.szoba}`;
      document.getElementById(`feltido${id}`).innerHTML = `Feltöltés időpontja: ${response.ido}`;
    }
  });
}

function deletePic(id, user) {
  fetch(`/pictures/${id}`, {
    method: 'DELETE',
  }).then((response) => {
    if (response.status === 204) {
      document.getElementById(`kep${id}`).style.display = 'none';
      document.getElementById(`success${id}`).style.display = 'block';
      document.getElementById(`remove${id}`).style.display = 'none';
    } else {
      document.getElementById(`success${id}`).style.display = 'block';
      document.getElementById(`success${id}`).innerHTML = `Sikertelen törlés. ${response.message}`;
    }
  });
}
