const express = require('express');
const path = require('path');
const eformidable =  require('express-formidable');
const fs = require('fs');
const db = require('../db/db');


const router = express.Router();


const uploadDir = path.join(__dirname, '../static/uploadDir');

// feltöltési mappa elkészítése
if (!fs.existsSync(uploadDir)) {
  fs.mkdirSync(uploadDir);
}

router.use('/uploadDir', express.static(uploadDir));

// formidable-lel dolgozzuk fel a kéréseket
router.use(eformidable({ uploadDir, keepExtensions: true }));

router.post('/upload_file', (request, response) => {
  const id = `${request.fields.hirdID}`;
  const allnev = `${request.files.fileToUpload.name}`;
  const type = `${request.files.fileToUpload.type}`;
  const nev = `${request.files.fileToUpload.path}`;
  if (allnev.length === 0) {
    response.render('error', { message: 'Nincs kiválasztott file. Próbálja újra. ' });
    try {
      fs.unlinkSync(nev);
      // file removed
    } catch (err) {
      response.render('serverError', { message: err });
    }
  } else if (type !== 'image/jpeg') {
    response.render('error', { message: 'A kiválasztott file nem kép. Próbálja újra. ' });
    try {
      fs.unlinkSync(nev);
      // file removed
    } catch (err) {
      response.render('serverError', { message: err });
    }
  } else {
    db.insertPicture(request, (err) => {
      if (err) {
        response.render('serverError', { message: err });
      } else {
        response.redirect(`/products?id=${id}`);
      }
    });
  }
});

module.exports = router;
