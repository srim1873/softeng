const express = require('express');
const cookieParser = require('cookie-parser');
const db = require('../db/db');
const getusername = require('../middleware/username');

const router = express.Router();
router.use(cookieParser());

router.delete('/pictures/:id', (request, response) => {
  const id = `${request.params.id}`;
  const username = getusername(request);
  db.findRealUser(id, (error, data) => {
    if (error) {
      response.status(500).json({ message: `Error while deleting picture with ID ${id}: ${error.message}` });
    } else {
      const realuser = data.tulaj;
      if (realuser === username) {
        db.deletePicture(id, (err) => {
          if (err) {
            response.status(500).json({ message: `Error while deleting picture with ID ${id}: ${error.message}` });
          } else {
            response.sendStatus(204);
          }
        });
      } else {
        response.status(500).json({ message: `Error while deleting picture with ID ${id}: ${error.message}` });
      }
    }
  });
});

module.exports = router;
