/* eslint-disable max-nested-callbacks */
const express = require('express');
const cookieParser = require('cookie-parser');
const jwt = require('jsonwebtoken');
const crypto = require('crypto');
const db = require('../db/db');


const secret = '1c28d07215544bd1b24fac3242352cacacad6c14a04';
const hashSize = 32,
  hashAlgorithm = 'sha512',
  iterations = 1000;


const router = express.Router();
router.use(cookieParser());

const checkHash = (password, hashWithSalt, callback) => {
  const expectedHash = hashWithSalt.substring(0, hashSize * 2),
    salt = Buffer.from(hashWithSalt.substring(hashSize * 2), 'hex');
  crypto.pbkdf2(password, salt, iterations, hashSize, hashAlgorithm, (err, binaryHash) => {
    if (err) {
      callback({ message: `Hashing unsuccessful: ${err.message}` });
    } else {
      const actualHash = binaryHash.toString('hex');
      if (expectedHash === actualHash) {
        callback(null);
      } else {
        callback({ message: 'Rossz jelszó.' });
      }
    }
  });
};

router.post('/login', (request, response) => {
  const user = `${request.body.felhasznalo}`;
  const password = `${request.body.password}`;
  db.findUserByName(request, (usererr, userdata) => {
    if (usererr) {
      response.render('serverError', { message: usererr });
    }
    if (userdata.length === 0) {
      response.render('error', { message: 'Nincs ilyen felhasználó. Próbálja újra, vagy regisztráljon.' });
    } else {
      const realpasswd = userdata.password;
      const isadmin = userdata.admin;
      checkHash(password, realpasswd, (error) => {
        if (error) {
          response.render('error', { message: error.message });
        }
        const token = jwt.sign({ user, isadmin }, secret);
        db.findAllProducts(request, (err, data) => {
          if (err) {
            response.render('serverError', { message: err });
          }
          if (data.length === 0) {
            response.render('error', { message: 'Nincs találat.' });
          } else {
            response.cookie('token', token);
            response.render('products', { hirdetes: data, user });
          }
        });
      });
    }
  });
});

module.exports = router;
