const express = require('express');
const cookieParser = require('cookie-parser');
const db = require('../db/db');
const authorization = require('../middleware/authorization');

const router = express.Router();
router.use(cookieParser());

// eslint-disable-next-line complexity
function validate(city, quarter, price, room, area) {
  // varos nev validalas
  if (!(/[A-ZÁÉÚŐÓÜÖÍ][a-z]*/.test(city) && city.length !== 0)) {
    return false;
  }

  // negyed nev validalas
  if (!(/[A-ZÁÉÚŐÓÜÖÍ][a-z]*/.test(quarter) && quarter.length !== 0)) {
    return false;
  }

  // ar validalas
  if (!(/[1-9][0-9]*/.test(price) && price.length !== 0)) {
    return false;
  }

  // szoba validalas
  if (!(/[1-9][0-9]*/.test(room) && room.length !== 0)) {
    return false;
  }

  // terulet validalad
  if (!(/[1-9][0-9]*/.test(area) && area.length !== 0)) {
    return false;
  }

  return true;
}

router.post('/submit_form', authorization(), (request, response) => {
  const city = `${request.body.varos}`;
  const quarter = `${request.body.negyed}`;
  const price = `${request.body.ar}`;
  const room = `${request.body.szoba}`;
  const area = `${request.body.terulet}`;
  if (validate(city, quarter, price, room, area)) {
    db.insertProduct(request, (err) => {
      if (err) {
        response.render('serverError', { message: err });
      } else {
        response.redirect('/');
      }
    });
  } else {
    response.render('error', { message: 'Nem megfelelo formatum. Próbálja újra.' });
  }
});

module.exports = router;
