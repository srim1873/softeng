const express = require('express');
const db = require('../db/db');

const router = express.Router();

router.get('/products/:id', (request, response) => {
  const id = `${request.params.id}`;
  db.findProduct(id, (findProdErr, productdata) => {
    if (findProdErr) {
      response.status(500).json({ message: `Error while showing details ${id}: ${findProdErr.message}` });
    }
    if (productdata.length === 0) {
      response.status(404).json({ message: `Product with ID ${id} not found.` });
    } else {
      const szobak = productdata.szoba;
      const feltido = productdata.feltoltesIdopont;
      const obj = { szoba: szobak, ido: feltido, error: null };
      response.json(obj);
    }
  });
});

module.exports = router;
