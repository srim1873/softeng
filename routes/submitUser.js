/* eslint-disable max-nested-callbacks */
const express = require('express');
const crypto = require('crypto');
const db = require('../db/db');

const router = express.Router();

const hashSize = 32,
  saltSize = 16,
  hashAlgorithm = 'sha512',
  iterations = 1000;

const createHash = (password, callback) => {
  crypto.randomBytes(saltSize, (err, salt) => {
    if (err) {
      return callback(new Error(err), null);
    }
    // hash készítése
    crypto.pbkdf2(password, salt, iterations, hashSize, hashAlgorithm, (cryptErr, hash) => {
      if (cryptErr) {
        return callback(new Error(err), null);
      }
      const hashWithSalt = Buffer.concat([hash, salt]).toString('hex');
      // a konkatenált hash-t és sót tárolnánk adatbázisban
      return callback(null, hashWithSalt);
    });
    return null;
  });
};

// eslint-disable-next-line complexity
function validate(email, username, password, passwordagain) {
  if (email.length === 0 || username.length === 0
        || password.length === 0 || passwordagain.length === 0) {
    return 1;
  }

  if (password.length < 8) {
    return 5;
  }

  if (password.localeCompare(passwordagain) !== 0) {
    return 4;
  }

  if (!username.match('^[A-Za-z0-9]+$')) {
    return 3;
  }

  if (!/^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/.test(email)) {
    return 2;
  }

  return 0;
}

router.post('/submitUser', (request, response) => {
  const email = `${request.body.email}`;
  const username = `${request.body.felhasznalo}`;
  const password = `${request.body.password}`;
  const passwordagain = `${request.body.passwordagain}`;

  const fail = validate(email, username, password, passwordagain);
  if (fail === 1) {
    response.render('error', { message: 'Minden *-al jelölt mező kitöltése kötelező.' });
  } else if (fail === 2) {
    response.render('error', { message: 'Az email cím formátuma helytelen.' });
  } else if (fail === 3) {
    response.render('error', { message: 'A felhasználonév formátuma helytelen. Csak számokat és betűket tartalmazhat.' });
  } else if (fail === 4) {
    response.render('error', { message: 'A két jelszó nem egyezik.' });
  } else if (fail === 5) {
    response.render('error', { message: 'A jelszó legalább 8 karakter hosszúságú kell legyen.' });
  }

  db.findUserByEmail(request, (findusErr, userdata) => {
    if (findusErr) {
      response.render('serverError', { message: findusErr });
    }
    if (userdata === null) {
      db.findUserByName(request, (Err, data) => {
        if (Err) {
          response.render('serverError', { message: Err });
        }
        if (data === null) {
          createHash(request.body.password,
            (error, hashedpasswd) => {
              if (error) {
                response.render('error', { message: error });
                return;
              }
              request.body.password = hashedpasswd;
              db.insertUser(request, (inserterr) => {
                if (inserterr) {
                  response.render('serverError', { message: inserterr });
                } else {
                  response.redirect('/');
                }
              });
            });
        } else {
          response.render('error', { message: 'A megadott felhasználónév már foglalt.' });
        }
      });
    } else {
      response.render('error', { message: 'A megadott emailcímhez már tartozik felhasználó.' });
    }
  });
});

module.exports = router;
