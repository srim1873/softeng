const express = require('express');
const cookieParser = require('cookie-parser');
const getusername = require('../middleware/username');
const authorization = require('../middleware/authorization');
const db = require('../db/db');

const router = express.Router();
router.use(cookieParser());

function sendData(request, response) {
  const id = `${request.query.id}`;
  db.deleteProduct(id, (err) => {
    if (err) {
      response.render('serverError', { message: err });
    } else {
      response.redirect('/');
    }
  });
}

router.get('/deleteProduct', authorization(['admin']), (request, response) => {
  const user = getusername(request);
  sendData(request, response, user);
});

module.exports = router;
