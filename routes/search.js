/* eslint-disable complexity */
/* eslint-disable max-lines-per-function */
const express = require('express');
const cookieParser = require('cookie-parser');
const getusername = require('../middleware/username');
const db = require('../db/db');

const router = express.Router();
router.use(cookieParser());

function sendResponse(data, err, response, user) {
  if (err) {
    response.render('serverError', { message: err });
  }
  if (data.length === 0) {
    response.render('error', { message: 'Nincs találat.' });
  } else {
    response.render('products', { hirdetes: data, user });
  }
}

function sendData(request, response, user) {
  const city = `${request.query.varosSearch}`;
  const quarter = `${request.query.negyedSearch}`;
  const minp = `${request.query.arMin}`;
  const maxp = `${request.query.arMax}`;

  // ha semmi sincs megadva
  if (city === '' && quarter === '' && minp === '' && maxp === '') {
    db.findAllProducts(request, (err, data) => {
      sendResponse(data, err, response, user);
    });
  }

  // ha csak a varos van megadva
  if (city !== '' && quarter === '' && minp === '' && maxp === '') {
    db.findByCity(request, (err, data) => {
      sendResponse(data, err, response, user);
    });
  }

  // ha csak a negyed van megadva
  if (city === '' && quarter !== '' && minp === '' && maxp === '') {
    db.findByQuarter(request, (err, data) => {
      sendResponse(data, err, response, user);
    });
  }

  // ha csak a min ar van megadva
  if (city === '' && quarter === '' && minp !== '' && maxp === '') {
    db.findByMinPrice(request, (err, data) => {
      sendResponse(data, err, response, user);
    });
  }

  // ha csak a max ar van megadva
  if (city === '' && quarter === '' && minp === '' && maxp !== '') {
    db.findByMaxPrice(request, (err, data) => {
      sendResponse(data, err, response, user);
    });
  }

  // ha a meg van adva a varos es negyed
  if (city !== '' && quarter !== '' && minp === '' && maxp === '') {
    db.findByCityQuarter(request, (err, data) => {
      sendResponse(data, err, response, user);
    });
  }

  // ha meg van adva a varos es a min ar
  if (city !== '' && quarter === '' && minp !== '' && maxp === '') {
    db.findByCityMinPrice(request, (err, data) => {
      sendResponse(data, err, response, user);
    });
  }

  // ha meg van adva a varos es a max ar
  if (city !== '' && quarter === '' && minp === '' && maxp !== '') {
    db.findByCityMaxPrice(request, (err, data) => {
      sendResponse(data, err, response, user);
    });
  }

  // ha meg van adva a negyed es a min ar
  if (city === '' && quarter !== '' && minp !== '' && maxp === '') {
    db.findByQuarterMinPrice(request, (err, data) => {
      sendResponse(data, err, response, user);
    });
  }
  // ha meg van adva a negyed es a max ar
  if (city === '' && quarter !== '' && minp === '' && maxp !== '') {
    db.findByQuarterMaxPrice(request, (err, data) => {
      sendResponse(data, err, response, user);
    });
  }

  // ha meg van adva a min ar es a max ar
  if (city === '' && quarter === '' && minp !== '' && maxp !== '') {
    db.findByMinPriceMaxPrice(request, (err, data) => {
      sendResponse(data, err, response, user);
    });
  }

  // ha meg van adva a varos negyed es min ar
  if (city !== '' && quarter !== '' && minp !== '' && maxp === '') {
    db.findByCityQuarterMinPrice(request, (err, data) => {
      sendResponse(data, err, response, user);
    });
  }

  // ha meg van adva a varos negyed es max ar
  if (city !== '' && quarter !== '' && minp === '' && maxp !== '') {
    db.findByCityQuarterMaxPrice(request, (err, data) => {
      sendResponse(data, err, response, user);
    });
  }

  // ha meg van adva a varos a min ar es max ar
  if (city !== '' && quarter === '' && minp !== '' && maxp !== '') {
    db.findByCityMinPriceMaxPrice(request, (err, data) => {
      sendResponse(data, err, response, user);
    });
  }

  // ha meg van adva a negyed a min ar es max ar
  if (city === '' && quarter !== '' && minp !== '' && maxp !== '') {
    db.findByQuarterMinPriceMaxPrice(request, (err, data) => {
      sendResponse(data, err, response, user);
    });
  }

  // ha minden meg van adva
  if (city !== '' && quarter !== '' && minp !== '' && maxp !== '') {
    db.findByAll(request, (err, data) => {
      sendResponse(data, err, response, user);
    });
  }
}

router.get('/search', (request, response) => {
  const user = getusername(request);
  sendData(request, response, user);
});


module.exports = router;
