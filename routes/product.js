const express = require('express');
const cookieParser = require('cookie-parser');
const db = require('../db/db');
const getusername = require('../middleware/username');

const router = express.Router();
router.use(cookieParser());

function sendData(request, response, user) {
  const id = `${request.query.id}`;
  let hird = '';
  let kep = '';
  db.findProduct(id, (findProdErr, productdata) => {
    if (findProdErr) {
      response.render('serverError', { message: findProdErr });
    } else if (productdata.length === 0) {
      response.render('error', { message: 'Nincs találat.' });
    } else {
      hird = productdata;
      let realuser = productdata.tulaj;
      if (realuser !== user) {
        realuser = null;
      }
      db.findPicture(id, (findPricerr, picturedata) => {
        if (findPricerr) {
          response.render('serverError', { message: findPricerr });
        } else {
          kep = picturedata;
          const model = {
            hird,
            kep,
            user,
            realuser,
          };
          response.render('productinfo', model);
        }
      });
    }
  });
}

router.get('/products', (request, response) => {
  const user = getusername(request);
  sendData(request, response, user);
});
module.exports = router;
