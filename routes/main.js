const express = require('express');
const cookieParser = require('cookie-parser');
const getusername = require('../middleware/username');
const db = require('../db/db');

const router = express.Router();
router.use(cookieParser());

function sendData(request, response, user) {
  db.findAllProducts(request, (error, data) => {
    if (error) {
      response.render('serverError', { message: error });
    }
    if (data.length === 0) {
      response.render('error', { message: 'Nincs találat.' });
    } else {
      response.render('products', { hirdetes: data, user });
    }
  });
}

router.get('/', (request, response) => {
  const user = getusername(request);
  sendData(request, response, user);
});

module.exports = router;
