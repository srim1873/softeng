const express = require('express');
const cookieParser = require('cookie-parser');
const db = require('../db/db');

const router = express.Router();
router.use(cookieParser());

router.post('/logout', (request, response) => {
  response.clearCookie('token');
  db.findAllProducts(request, (error, data) => {
    if (error) {
      response.render('serverError', { message: error });
    }
    if (data.length === 0) {
      response.render('error', { message: 'Nincs találat.' });
    } else {
      response.render('products', { hirdetes: data });
    }
  });
});

module.exports = router;
