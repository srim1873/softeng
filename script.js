const express = require('express'),
  path = require('path'),
  hbs = require('hbs'),
  app = express();

const apiRoutes = require('./api');
const mainRouter = require('./routes/main');
const submitForm = require('./routes/submitProduct');
const uploadFile = require('./routes/submitPicture');
const search = require('./routes/search');
const productInfo = require('./routes/product');
const register = require('./routes/register');
const submitUser = require('./routes/submitUser');
const login = require('./routes/login');
const logout = require('./routes/logout');
const deleteProduct = require('./routes/deleteProduct');
const showdetail = require('./routes/showdetail');
const deletePic = require('./routes/deletePicture');
const errorMiddleware = require('./middleware/error');


app.use(express.static(path.join(__dirname, 'static')));
app.use(express.urlencoded({ extended: true }));
app.use('/api', apiRoutes);

hbs.registerPartials(path.join(__dirname, '/views/partials'));
app.set('view engine', 'hbs');
app.set('views', path.join(__dirname, 'views'));

app.get('/', mainRouter); // fo oldal
app.post('/submit_form', submitForm); // form1 -  hirdetes validalasa es feltoltese (csak felhasznalo)
app.post('/submitUser', submitUser); // uj felhasznalo
app.post('/upload_file', uploadFile); // form2 - kep feltoltese (csak felhasznalo)
app.get('/search', search); // form 3 - kereses
app.get('/products/:id', showdetail); // aszinkron reszletek
app.delete('/pictures/:id', deletePic); // aszinkron torles
app.get('/products', productInfo); // reszletes informacio
app.get('/register', register); // regisztracio oldal
app.post('/login', login); // bejelentkezes
app.post('/logout', logout); // kijelentkezes
app.get('/deleteProduct', deleteProduct); // hirdetes torlese (csak admin)


app.use(errorMiddleware);

app.listen(8080, () => { console.log('Server listening on http://localhost:8080/ ...'); });
